package com.classpath.test;

import com.classpath.junit.SavingsAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SavingsAccountTest {

    private SavingsAccount savingsAccount;

    @BeforeEach
    public void setUp() throws Exception {
        savingsAccount = new SavingsAccount(25_000);
    }

    @Test
    public void testInitialBalance(){
        assertEquals(savingsAccount.getCurrentBalance(), 25_000);
    }

    @Test
    public void testDepositMethod(){
        savingsAccount.deposit(20_000);
        assertEquals(savingsAccount.getCurrentBalance(), 45_000);
    }

    @Test
    public void testWithdrawMethod(){
        savingsAccount.deposit(20_000);
        savingsAccount.deposit(10_000);
        assertEquals(savingsAccount.getCurrentBalance(), 55_000);
    }

    @Test
    public void testInvalidWithdrawMethod(){
        savingsAccount.deposit(20_000);
        assertEquals(savingsAccount.getCurrentBalance(), 45_000);
        double amountWithdrawn = savingsAccount.withdraw(2_000);
        assertEquals(savingsAccount.getCurrentBalance(), 43_000);
        assertEquals(amountWithdrawn, 2000);
    }
}