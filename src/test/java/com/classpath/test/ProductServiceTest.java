package com.classpath.test;

import com.classpath.mock.ProductDAO;
import com.classpath.mock.ProductService;
import com.classpath.model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Properties;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @Mock
    ProductDAO productDAO;

    ProductService productService;

    @BeforeEach
    public void setUp() throws Exception {
        productService  = new ProductService(productDAO);
    }

    @Test
    public void test()  {
        assertNotNull(productDAO);

        when(productDAO.execute(anyString())).thenReturn("result");

        String result = productService.result("query");

        assertNotNull(result);

        assertEquals(result, "result");
    }

    @Test
    public void ensureMockitoReturnsTheConfiguredValue() {
        // define return value for method getUniqueId()
        when(productDAO.getProductId()).thenReturn(44);
        // use mock in test....
        assertEquals(productService.toString(), "Product Id is : 44");
    }

    @Test
    public void testMockitoThrows() {
        Properties properties = new Properties();
        Properties spyProperties = spy(properties);
        //set the expectation
        doReturn("org.jdbc.driver")
                .when(spyProperties)
                .get("db.driver.class");

        String value = (String) spyProperties.get("db.driver.class");
        assertEquals("org.jdbc.driver", value);
    }

    @Test
    void testStaticMethod() {
        try (MockedStatic<ProductDAO> mockedStatic = Mockito.mockStatic(ProductDAO.class)) {

            mockedStatic.when(() -> ProductDAO.getCurrentEnvironement()).thenReturn("QA");

            mockedStatic.when(() -> ProductDAO.getCurrentEnvironement()).thenReturn("PROD");

            String environement1 = ProductDAO.getCurrentEnvironement();

            assertEquals("PROD", environement1);

            String environement2 = ProductDAO.getCurrentEnvironement();

            //assertEquals("PROD", environement2);
        }
    }

    @Test
    public void testVerify(@Mock ProductDAO productDAO)  {
        // create and configure mock
        Product iPhone = new Product(12, "IPhone", 65_000 );
        // call method testing on the mock with parameter 12
        productDAO.saveProduct(iPhone);

        productDAO.fetchProductById(12);

        productDAO.fetchProductById(12);

        // now check if method testing was called with the parameter 12
        verify(productDAO).saveProduct(ArgumentMatchers.eq(iPhone));

        // was the method called twice?
        verify(productDAO, times(2)).fetchProductById(12);

        // other alternatives for verifiying the number of method calls for a method
        verify(productDAO, atLeastOnce()).saveProduct(iPhone);
        verify(productDAO, atLeast(2)).fetchProductById(12);
        verifyNoMoreInteractions(productDAO);
    }
}