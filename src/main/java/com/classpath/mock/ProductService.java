package com.classpath.mock;

public class ProductService {
    private ProductDAO productDAO;

    public ProductService(ProductDAO database) {
        this.productDAO = database;
    }

    public String result(String query) {
        boolean flag = validateData(query);
        if (flag) {
            return productDAO.execute(query);
        }
        return null;
    }

    private boolean validateData(String data){
        if (data == null) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "Product Id is : " + String.valueOf(productDAO.getProductId());
    }
}