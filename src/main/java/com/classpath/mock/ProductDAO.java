package com.classpath.mock;

import com.classpath.model.Product;

public class ProductDAO {

    private static String ENVRIONMENT = "DEV";

    private Product product;

    public int getProductId() {
        return (int) Math.abs(Math.random());
    }

    public String execute(String query) {
        return "Hello world !!";
    }

    public static String getCurrentEnvironement() {
        return ENVRIONMENT;
    }

    public void saveProduct(Product product){
        this.product = product;
    }

    public Product fetchProductById(long id){
        return this.product;
    }
}