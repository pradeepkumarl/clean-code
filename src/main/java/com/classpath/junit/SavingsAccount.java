package com.classpath.junit;

public class SavingsAccount {

    private double balance = 20_000;

    private static double MIN_BALANCE = 10_000;

    public SavingsAccount(double initialBalance){
        this.balance = initialBalance;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public double withdraw(double amount){
        if ( amount < this.balance && this.balance - amount >= MIN_BALANCE) {
            this.balance -= amount;
            return amount;
        }
        return 0;
    }

    public double getCurrentBalance(){
        return this.balance;
    }
}