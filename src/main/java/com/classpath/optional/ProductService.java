package com.classpath.optional;

import com.classpath.model.Product;

import java.util.*;
import java.util.stream.Collectors;

public class ProductService {

    public static void main(String[] args) {

        ProductDAO productDAO = new ProductDAO();

        Set<Product> set = productDAO.fetchAllProducts();

        Optional<Product> optionalProduct = productDAO.fetchProductById(588);

        if(optionalProduct.isPresent()){
            Product product = optionalProduct.get();
        }

        Product product = productDAO.fetchProductById(12)
                                .orElseThrow(() -> new IllegalArgumentException("Invalid id"));
         Product product2 = productDAO
                 .fetchProductById(12)
                 .orElseGet(() -> new Product(12, "Dummy", 23_000));

    }
}