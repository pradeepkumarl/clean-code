package com.classpath.optional;

import com.classpath.model.Product;

import java.util.Optional;
import java.util.Set;

public class ProductDAO {
    private static Set<Product> products = Set.of(
            new Product(1, "IPhone", 65_000),
            new Product(2, "Macbook-Pro", 2_15_000),
            new Product(3, "IPad", 29_000),
            new Product(4, "IPad-Pro", 49_000),
            new Product(5, "MacBook-Air", 50_000)

    );
    public Set<Product> fetchAllProducts() {
      return products;
    }

    public Optional<Product> fetchProductById(long id){
        return products.parallelStream().filter(product -> product.getId() == id).findAny();
    }
}