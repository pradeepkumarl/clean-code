package com.classpath.streams;

import com.classpath.model.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;

public class GroupByDemo {

    public static void main(String[] args) {
        List<Product> products = Arrays.asList(
                new Product(1, "IPhone", 65_000),
                new Product(2, "Macbook-Pro", 2_15_000),
                new Product(3, "IPad", 29_000),
                new Product(4, "Samsung-S20", 50_000),
                new Product(3, "OPPO", 25_000));

        Map<String, List<Product>> groupProductsByName = groupProductsByName(products);

        processProducts(products, groupProductsByName);
    }

    private static void processProducts(List<Product> products, Map<String, List<Product>> productsMap) {
        productsMap.forEach(printProductsMap(products));
    }

    private static BiConsumer<String, List<Product>> printProductsMap(List<Product> products) {
        return (name, productList) -> printProductsByName(products, name);
    }

    private static void printProductsByName(List<Product> products, String name) {
        System.out.println("Name: " + name + "," + products.stream().map(Product::getName).collect(joining(",")));
    }

    private static Map<String, List<Product>> groupProductsByName(List<Product> products) {
        Map<String,List <Product>> productsByName = products.stream().collect(groupingBy(Product::getName));
        return productsByName;
    }

}