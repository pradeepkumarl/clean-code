package com.classpath.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import static java.util.Comparator.reverseOrder;

public class StreamsDemo {

    public static void main(String[] args) {

        List<Integer> ages = Arrays.asList(22,44,22,11,22,24,55,67,55,11,44,17,19,20);
        System.out.println("Initial size : "+ ages.size());
        //filter all the adults and print the first 5 in the descreasing order.

        Predicate<Integer> ageGreaterThan18 = (age) -> age >= 18;

        ages.stream().filter(ageGreaterThan18).distinct().sorted(reverseOrder()).limit(4).forEach(System.out::println);

    }
}