package com.classpath.methodaround.end;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MethodAroundDemo {

    public static void main(String[] args) throws IOException {
        String line = processFile("E:\\projects\\clean_code\\src\\main\\java\\com\\classpath\\methodaround\\start\\products.txt");
        System.out.println(line);
    }


    private static String processFile(String fileName) throws IOException {
        //create a buffered reader
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            //file processing
            return reader.readLine();
        }
    }
}