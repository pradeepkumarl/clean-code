package com.classpath.methodaround.end;

import java.util.function.Consumer;

public class Resource {

    private Resource() {
        System.out.println("Creating Resource");
    }

    // Closing Resources
    private void close(){
        System.out.println("Closing Resource");
    }

    public Resource operation(String input){
        System.out.println("Operation with resource input: "+ input);
        return this;
    }

    public static void apply(Consumer<Resource> resourceConsumer){
        Resource resource = new Resource();
        try{
            resourceConsumer.accept(resource);
        }
        finally {  //Cleaning up the resource
            resource.close();
        }
    }
    public static void main(String[] args) {
        //Chaining operations
        Resource.apply(resource -> resource
                                    .operation("1")
                                    .operation("2"));
    }
}