package com.classpath.methodaround.start;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Consumer;

public class MethodAroundDemo {

    private static final String FILE_PATH = "E:\\projects\\clean_code\\src\\main\\java\\com\\classpath\\methodaround\\start\\products.txt";

    public static void main(String[] args) throws IOException {

        Consumer<String> customReader = MethodAroundDemo::printLine;

        processFile( FILE_PATH, customReader);
    }

    private static void processFile(String filePath, Consumer<String> customFileProcessor) throws IOException {
        //create a buffered reader
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))){
            //file processing
            String line = reader.readLine();
            while(line != null) {
                customFileProcessor.accept(line);
                line = reader.readLine();
            }
        }
    }

    private static void printLine(String line) {
            System.out.println(line);
    }
}
