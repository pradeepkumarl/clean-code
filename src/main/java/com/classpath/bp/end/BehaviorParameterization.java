package com.classpath.bp.end;

import com.classpath.model.Product;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import static java.util.stream.Collectors.toList;

public class BehaviorParameterization {

    //Use Arrays.asList().. method
    static List<Product> products = Arrays.asList(
            new Product(1, "IPhone", 65_000),
            new Product(2, "Macbook-Pro", 2_15_000),
            new Product(3, "IPad", 29_000)
    );

    public static void main(String[] args) {

        Comparator<Product> nameAscComparator = ( product1, product2) -> product1.getName().compareTo(product2.getName());

        Comparator<Product> nameDescComparator = ( product1, product2) -> product2.getName().compareTo(product1.getName());


        productsSortedByName(products, nameAscComparator);
        products.forEach(System.out::println);
    }

    public static List<Product> filterProductsByMinPrice(List<Product> products){
        return products.stream()
                    .filter((product) ->  product.getPrice() < 30_000)
                    .collect(toList());
    }

    public static List<Product> filterProductsByName(List<Product> products, String name){
        return products.stream().filter(product ->  product.getName().equalsIgnoreCase(name)).collect(toList());
    }

    //sorting the list
    public static void productsSortedByName(List<Product> products, Comparator<Product> comparator){
        products.sort(comparator);
    }
    //add the comparator
}






