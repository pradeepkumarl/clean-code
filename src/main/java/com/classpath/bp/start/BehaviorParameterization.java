package com.classpath.bp.start;

import com.classpath.model.Product;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BehaviorParameterization {

    //Use List.of operator or Arrays.asList().. method
    List<Product> products = Arrays.asList(
            new Product(1, "IPhone", 65_000),
            new Product(2, "Macbook-Pro", 2_15_000),
            new Product(3, "IPad", 29_000)
    );
    public static void main(String[] args) {
    }

    //imperative style of processing the products - 1st attempt
    /*
        hard coding the value
        conditions is embedded inside the code
     */
    public static List<Product> filterProductsByName(List<Product> products){
        List<Product> filteredProducts = new ArrayList<>();
        for(Product product: products) {
            if (product.getPrice() < 30_000) {
                filteredProducts.add(product);
            }
        }
        return filteredProducts;
    }

    //imperative style of processing the products - 2nd attempt
    public static List<Product> filterProductsByNameParam(List<Product> products, double price){
        List<Product> filteredProducts = new ArrayList<>();
        for(Product product: products) {
            if (product.getPrice() < price) {
                filteredProducts.add(product);
            }
        }
        return filteredProducts;
    }
}


