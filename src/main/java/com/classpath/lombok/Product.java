package com.classpath.lombok;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(of = "id")
public class Product {


    private long id;
    @NonNull
    private String name;

    private double price;

}