package com.classpath.lombok;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class ProductService {
    public static void main(String[] args) {
        log.info("Came inside the main method {}", "hello");
        Product iPad = Product.builder().id(12).name("iPhone").price(30000).build();

        System.out.println(iPad);
    }
}