package com.classpath.predicates;

import com.classpath.model.Product;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PredicatesDemo {
    //Use Arrays.asList().. method
    static List<Product> products = Arrays.asList(
            new Product(1, "IPhone", 65_000),
            new Product(2, "Macbook-Pro", 2_15_000),
            new Product(3, "IPad", 29_000),
            new Product(4, "Samsung-S20", 50_000),
            new Product(3, "OPPO", 25_000)
    );

    public static boolean productIsPhone(Product product){
        return product.getName().equalsIgnoreCase("IPhone");
    }

    public static void main(String[] args) {
        products.stream().filter(notAppleProduct).forEach(printConsumer);
    }



    static Predicate<Product> lesssThan100K = (product) -> product.getPrice() < 10_000;


    static Predicate<Product> moreThan100K = lesssThan100K.negate();


    Predicate<Product> appleProductsLessThan100K = appleProduct.and(lesssThan100K).and(iPad);

    static Predicate<Product> iPhone = PredicatesDemo::productIsPhone;

    static Predicate<Product> macBookPro = PredicatesDemo::testProductIsMacbookPro;

    static Predicate<Product> iPad = (product) -> product.getName().equalsIgnoreCase("IPad");


    static Predicate<Product> appleProduct = iPhone.or(macBookPro).or(iPad);

    static Predicate<Product> notAppleProduct =  appleProduct.negate();

    static Consumer<Product> printConsumer = System.out::println;


    private static boolean testProductIsMacbookPro(Product product) {
        return product.getName().equalsIgnoreCase("Macbook-Pro");
    }
}